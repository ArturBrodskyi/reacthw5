import ItemList from "../itemList";
import Counter from "../Cart&FavCounter";
const HomeRoute = ({ items = [], FavouriteFn = () => {}, openModal = () => {}, addToCart = () => {}}) => {
  return (
    <>
      <ItemList
        items={items}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
        addToCart={addToCart}
      ></ItemList>
    </>
  );
};

export default HomeRoute;
