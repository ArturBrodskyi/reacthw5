import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isModalOpen: false,
  isDeleteModalOpen: false,
  isCheckoutModalOpen: false,
  selectedItem: '',
};

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    openModalSlice(state, action) {
      state.isModalOpen = true;
      state.selectedItem = action.payload
    },
    closeModalSlice(state, action){
        state.isModalOpen = false;
        state.selectedItem = action.payload
    },
    openDeleteModalSlice(state, action){
        state.isDeleteModalOpen = true;
        state.selectedItem = action.payload
    },
    closeDeleteModalSlice(state, action){
        state.isModalOpen = false;
        state.selectedItem = action.payload
    },
    openCheckoutModal(state, action){
      state.isCheckoutModalOpen = true
    },
    closeCheckoutModal(state, action){
      state.isCheckoutModalOpen = false
    }
  },
});

export const { openModalSlice, closeModalSlice, closeDeleteModalSlice , openDeleteModalSlice, closeCheckoutModal, openCheckoutModal} = modalSlice.actions;
export default modalSlice.reducer;
