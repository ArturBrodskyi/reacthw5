import { useDispatch } from "react-redux";
import ItemCartList from "../ItemCartList";
import { openCheckoutModal } from "../redux/modalSlice";

const CartRoute = ({ cart, removeFromCart, openDeleteModal }) => {
const dispatch = useDispatch()
  return (
    <>
      <ItemCartList
        items={cart}
        removeFromCart={removeFromCart}
        openDeleteModal={openDeleteModal}
      ></ItemCartList>

      <button className="checkout-btn" onClick ={() => (dispatch(openCheckoutModal()))}>Checkout</button>
    </>
  );
};

export default CartRoute;
